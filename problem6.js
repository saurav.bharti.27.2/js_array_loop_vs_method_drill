
const got = require('./data')

function surnameWithS(){

    const peopleWithSInSurname = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                let nameOfIndividual = individualPeople.name

                let firstNameAndSurname = nameOfIndividual.split(' ')

                let last = firstNameAndSurname.length -1

                let surname = firstNameAndSurname[last]

                if(surname[0] === 'S'){

                    peopleWithSInSurname.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithSInSurname
}

console.log(surnameWithS())