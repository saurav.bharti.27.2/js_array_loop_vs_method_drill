
const got = require('./data')


function peopleByHouses(){

    const peopleByEachHouse = {}
    
    for (const houses in got){
    
        for (const eachHouse in got[houses]){
    
            peopleByEachHouse[got[houses][eachHouse].name] = got[houses][eachHouse].people.length
        }
    }

    return peopleByEachHouse;

}


console.log(peopleByHouses())