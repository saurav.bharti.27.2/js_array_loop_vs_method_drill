
const got = require('./data')

function nameWithA(){

    const peopleWithAInNames = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                if((individualPeople.name.toLowerCase()).includes('a')){

                    peopleWithAInNames.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithAInNames;

}

console.log(nameWithA())