
const got = require('./data')

function nameWithS(){

    const peopleWithSInNames = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                if((individualPeople.name.toLowerCase()).includes('s')){

                    peopleWithSInNames.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithSInNames;

}

console.log(nameWithS())


