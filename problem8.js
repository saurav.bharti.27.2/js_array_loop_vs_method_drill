
const got = require('./data')

function peopleNameOfAllHouses(){

    const objectWithHouseAndNames = {}

    for(const houses in got){

        for(const eachHouse in got[houses]){

            if( !objectWithHouseAndNames.hasOwnProperty( got[houses][eachHouse].name ) ){

                objectWithHouseAndNames[got[houses][eachHouse].name] = [] 
            }

            for(const individualPeople of got[houses][eachHouse].people){

                objectWithHouseAndNames[got[houses][eachHouse].name].push(individualPeople.name)

            }

        }

    }
    
    return objectWithHouseAndNames

}

console.log(peopleNameOfAllHouses())