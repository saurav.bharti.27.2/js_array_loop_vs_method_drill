

const got = require('./data')

function surnameWithA(){

    const peopleWithAInSurname = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                let nameOfIndividual = individualPeople.name

                let firstNameAndSurname = nameOfIndividual.split(' ')

                let surname = firstNameAndSurname[1]

                if(surname[0] === 'A'){

                    peopleWithAInSurname.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithAInSurname
}

console.log(surnameWithA())