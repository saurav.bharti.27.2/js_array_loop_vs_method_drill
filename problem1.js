
const got = require('./data')


function countAllPeople(){

    let totalPeople = 0;
    
    for( const houses in got){
    
        for( const eachHouse in got[houses]){
    
            totalPeople += got[houses][eachHouse].people.length
    
        }
    
    }

    return totalPeople
}

console.log(countAllPeople())